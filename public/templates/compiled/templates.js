(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['content'] = template({"1":function(container,depth0,helpers,partials,data,blockParams) {
    return "    <li>"
    + container.escapeExpression(container.lambda(blockParams[0][0], depth0))
    + "</li>\n";
},"3":function(container,depth0,helpers,partials,data,blockParams) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <li>"
    + alias2(alias1(blockParams[0][1], depth0))
    + " -- "
    + alias2(alias1(((stack1 = blockParams[0][0]) != null ? lookupProperty(stack1,"status") : stack1), depth0))
    + "</li>\n";
},"5":function(container,depth0,helpers,partials,data,blockParams) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <li>"
    + alias2(alias1(((stack1 = blockParams[0][0]) != null ? lookupProperty(stack1,"a") : stack1), depth0))
    + " -- "
    + alias2(alias1(((stack1 = blockParams[0][0]) != null ? lookupProperty(stack1,"b") : stack1), depth0))
    + "</li>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data,blockParams) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h1>"
    + alias4(((helper = (helper = lookupProperty(helpers,"text") || (depth0 != null ? lookupProperty(depth0,"text") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data,"blockParams":blockParams,"loc":{"start":{"line":2,"column":4},"end":{"line":2,"column":14}}}) : helper)))
    + "</h1>\n<h2>Person</h2>\n<p>Name: "
    + alias4(alias5(((stack1 = (depth0 != null ? lookupProperty(depth0,"object") : depth0)) != null ? lookupProperty(stack1,"name") : stack1), depth0))
    + ", age: "
    + alias4(alias5(((stack1 = (depth0 != null ? lookupProperty(depth0,"object") : depth0)) != null ? lookupProperty(stack1,"age") : stack1), depth0))
    + " </p>\n<p>Be <b>bold</b> in stating your key points. Put them in a list: </p>\n<ul>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"array") : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 1, blockParams),"inverse":container.noop,"data":data,"blockParams":blockParams,"loc":{"start":{"line":9,"column":2},"end":{"line":11,"column":11}}})) != null ? stack1 : "")
    + "</ul>\n\n<ul>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"objects") : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 2, blockParams),"inverse":container.noop,"data":data,"blockParams":blockParams,"loc":{"start":{"line":16,"column":2},"end":{"line":18,"column":11}}})) != null ? stack1 : "")
    + "</ul>\n\n<ul>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"arrayObjects") : depth0),{"name":"each","hash":{},"fn":container.program(5, data, 2, blockParams),"inverse":container.noop,"data":data,"blockParams":blockParams,"loc":{"start":{"line":23,"column":2},"end":{"line":25,"column":11}}})) != null ? stack1 : "")
    + "</ul>\n\n<p>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"tag") || (depth0 != null ? lookupProperty(depth0,"tag") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tag","hash":{},"data":data,"blockParams":blockParams,"loc":{"start":{"line":29,"column":3},"end":{"line":29,"column":14}}}) : helper))) != null ? stack1 : "")
    + "</p>\n\n<div id=\"block-content\"></div>";
},"useData":true,"useBlockParams":true});
templates['content1'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<p>Hello there</p>";
},"useData":true});
})();